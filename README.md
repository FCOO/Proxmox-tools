# Proxmox Tools for GEOMETOC

Small programs we use here at GEOMETOC/FCOO
 
We are using the python library proxmoxer direct in this code.
Check the website/git repository for information about proxmoxer.
https://github.com/proxmoxer/proxmoxer

proxmoxer is using a MIT license

Our tools are using a 3. part BSD license.

## Getting started
1) clone the repository:

2) Fill out the proxmox_cluster.json file with credintials

3) Execute a script:
python Proxmox-list-owner-information.py 
or
python Proxmox-vm-with-ISO-mounted.py