from proxmoxer import ProxmoxAPI

import json

# Opening JSON configuration file.
json_configuration_file = open('proxmox_cluster.json')

proxmox_cluster = json.load(json_configuration_file)

json_configuration_file.close()

proxmox = ProxmoxAPI(host=proxmox_cluster['host'], user=proxmox_cluster['user'],
                     password=proxmox_cluster['password'], verify_ssl=False)

vm_list=[]

for vm in proxmox.cluster.resources.get(type='vm'):
    my_vm = proxmox.get('nodes', vm['node'], 'qemu', vm['vmid'],'config')

    if 'ide2' in my_vm.keys():
        if my_vm['ide2'].find("none") < 0:
            print( my_vm['name'] )
            print( my_vm['ide2'] )
        

    if 'cdrom' in  my_vm.keys():
        if my_vm['cdrom'].find("none") < 0:
            print( my_vm['name'] )
            print( my_vm['cdrom'] )
