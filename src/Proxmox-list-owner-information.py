from proxmoxer import ProxmoxAPI

import json

# Opening JSON configuration file.
json_configuration_file = open('proxmox_cluster.json')

proxmox_cluster = json.load(json_configuration_file)

json_configuration_file.close()

proxmox = ProxmoxAPI(host=proxmox_cluster['host'], user=proxmox_cluster['user'],
                     password=proxmox_cluster['password'], verify_ssl=False)

vm_list=[]

for vm in proxmox.cluster.resources.get(type='vm'):
    # We are only listing qemu based vm.
    my_vm = proxmox.get('nodes', vm['node'], 'qemu', vm['vmid'],'config')

    owner_string = "NO OWNER"
    if 'description' in my_vm.keys():
        vm_description = my_vm['description'].lower()
        index_owner = vm_description.find("owner")
        if index_owner >= 0:
            vm_owner_string = my_vm['description'][index_owner:]
            index_owner_end = vm_owner_string.find("\n")
            if index_owner_end >= 0:
                owner_string = vm_owner_string[:index_owner_end]
            else:
                owner_string = vm_owner_string
    vm_list.append(  my_vm['name'] + " || " + owner_string ) 

# Sort our list of vm to make it ease to find one.
vm_list.sort()


# We print out in a moinmoin friendly fashion
for vm_with_owner in vm_list:
    print( "|| " + vm_with_owner + " ||") 
